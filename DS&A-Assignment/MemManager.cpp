/* 
 * File:   Memory Manager Class.cpp
 * Author: Daniel Ormeño - S2850944
 * Griffith University - Gold Coast Campus
 * Data Structures & Algorithms Assignment 1
 *  MemManager Class and related methods
 */

#include "linkedList.cpp"

class MemManage {
private:
    int size; // Maximum Size of the memory array
    char * memoryArray; // Pointer to the Memory Array
    linkedList freeMemFragmentsLL; // A linked list for free memory fragments
    linkedList usedMemFragmentsLL; // A linked list for the allocated memory fragments
    linkedList auxLL;
    
public:
    
    //friend MemManage& operator=(MemManage& memoryBlock);
    //friend std::ostream& operator<<(std::ostream& os, const MemManage& memoryBlock);
    
    // Constructors
public:

    MemManage(int s) {
        size=s;
        memoryArray = new char [size]; // Allocates memory on the stack for a char array
        for (int i = 0; i < size; i++) { // New array is initialized as empty memory block
            memoryArray[i] = '\0';
        }
        //Free fragment of memory of size of array.
        freeMemFragmentsLL.updateNode(freeMemFragmentsLL.getHead(), size, memoryArray);
        freeMemFragmentsLL.unUsed = false;
    };

    ~MemManage() {
        //delete size;
        delete [] memoryArray;
        freeMemFragmentsLL.deleteAll();
        freeMemFragmentsLL.setHead(NULL);
        usedMemFragmentsLL.deleteAll();
        usedMemFragmentsLL.setHead(NULL);
        auxLL.deleteAll();
        auxLL.setHead(NULL);
    };

    // Methods

    void Dump() {
        for (int i = 0; i < 10; i++) {
            memoryArray[i]='a';
            printf(" %02X ", (unsigned)(unsigned char) memoryArray[i] );
        }
    };

    // Returns the total amount of free memory in all free fragments

    int Avail() {
        int result = freeMemFragmentsLL.sizeOfMemoryFragments();
        return result;
    };

    // Returns the total amount of used memory in all used fragments

    int used() {
        int result = usedMemFragmentsLL.sizeOfMemoryFragments();
        return result;
    };

    // Returns Pointer to Allocated Memory

    char * Alloc(int size) {

        node <char> * allocFragment = NULL;
        int newSizeOfFragment;

        //Checks for free memory size
        if (Avail() >= size) {
            allocFragment = freeMemFragmentsLL.smallestMemoryFragment(size); // Returns pointer to allocated memory fragment
            newSizeOfFragment = (allocFragment->segmentSize) - size;
        } else return NULL; // returns NULL if there is not enough free space.

        //Removes free memory from "free memory" linked list
        //RECUERDA AGREGAR CONDICION QUE VERIFIQUE QUE EL NUEVO TAMANO DEL FRAGMENTO SEA MAYOR A 0
        freeMemFragmentsLL.updateNode(allocFragment, newSizeOfFragment, allocFragment->segmentPtr + size);

        //Allocates node in "used memory" linked list.
        if (usedMemFragmentsLL.unUsed) {
            usedMemFragmentsLL.updateNode(usedMemFragmentsLL.getHead(), size, (allocFragment->segmentPtr) - size);
            usedMemFragmentsLL.unUsed = false;
        } else {
            usedMemFragmentsLL.insertNodeAtBeginning(size, (allocFragment->segmentPtr) - size);
        }

        return allocFragment->segmentPtr - size; //Returns the address of the allocated memory, not the new address of the node
    };

    // Enlarges allocated size if possible, if not assigns a new fragment of requested size.

    char * Realloc(char* ptr, int size) {

        node <char> * usedNodePtr = NULL;
        node <char> * foundNodePtr = NULL;
        char * auxPtr;
        int a;
        char * result;
        // Finds Address of Used of fragment to be reallocated and determines the address of the next element of the array
        usedNodePtr = usedMemFragmentsLL.findNode(ptr);
        
        if (size > Avail()) {
            std::cout << "Error: Not enough Memory Available" << std::endl;
            return NULL;
        } else if (usedNodePtr->segmentSize < size) {

            auxPtr = ptr + (usedNodePtr->segmentSize); // auxPtr points to next item in the array to check if its free
            foundNodePtr = freeMemFragmentsLL.findNode(auxPtr); //NUll if auxPtr is not found in free LL, Node address if found
            a = size - (usedNodePtr->segmentSize); //extra space needed to enlarge.

            if (foundNodePtr != NULL && a <= foundNodePtr->segmentSize) {

                usedMemFragmentsLL.updateNode(usedNodePtr, size, usedNodePtr->segmentPtr);
                if (a < foundNodePtr->segmentSize) {
                    freeMemFragmentsLL.updateNode(foundNodePtr, foundNodePtr->segmentSize - a, foundNodePtr->segmentPtr + a);
                } else {
                    freeMemFragmentsLL.deleteNode(foundNodePtr); //deletes node if all memory has been used
                }
                
                result = usedNodePtr->segmentPtr;

            } else {
                freeMemFragmentsLL.insertNodeAtBeginning(usedNodePtr->segmentSize, usedNodePtr->segmentPtr);
                usedMemFragmentsLL.deleteNode(usedNodePtr);
                result = Alloc(size);
            }
            return result;

        } else {
            std::cout << "Error: memory fragment is larger than requested new size, already large enough" << std::endl;
            return NULL;
        }
        freeMemFragmentsLL.mergeNodes();
    };

    void Free(char * ptr) {
        node <char> * nodePtr = usedMemFragmentsLL.findNode(ptr); // Finds address of node containing memory fragment
        std::cout << nodePtr->segmentSize << " Bytes freed from address " << static_cast<void*> (ptr) << std::endl;
        freeMemFragmentsLL.insertNodeAtBeginning(nodePtr->segmentSize, ptr);
        usedMemFragmentsLL.deleteNode(nodePtr);
    };
    
    void Compact () {
        
        int numberOfNodes = usedMemFragmentsLL.countNodes(); 
        node <char> * nodePtr;
        int index=0; // updates index of array to set segmentPtr
        int usedSize; // to determine size of remaining free memory
        
        for (int i=0; i<numberOfNodes; i++){
            //Finds the address of the node that has the largest address value
            nodePtr=usedMemFragmentsLL.findNode(usedMemFragmentsLL.largestMemoryAddress(&memoryArray[0]));
            // Inserts new node at beginning of the aux list (push)
            auxLL.insertNodeAtBeginning(nodePtr->segmentSize, &memoryArray[index]);
            index+=nodePtr->segmentSize;
            // Deletes node from used list to avoid repeated largest address value.
            usedMemFragmentsLL.deleteNode(nodePtr);
        }
        usedMemFragmentsLL.deleteAll(); 
        usedMemFragmentsLL.setHead(auxLL.getHead()); // head of used list points to aux list.
        auxLL.setHead(NULL); // avoids dereferencing issue
        auxLL.deleteAll();
        
        usedSize=usedMemFragmentsLL.sizeOfMemoryFragments();
        
        //freeMemFragmentsLL.deleteAll();
        auxLL.setHead(freeMemFragmentsLL.getHead()); // avoids dereferencing issue
        freeMemFragmentsLL.setHead(new node <char>); // Creates a new node on head of list
        freeMemFragmentsLL.updateNode(freeMemFragmentsLL.getHead(), size-usedSize,&memoryArray[usedSize]);
        auxLL.deleteAll(); 
    }

    void countFragments() {
        std::cout << "Number of free memory Fragments: " << freeMemFragmentsLL.countNodes() << std::endl;
        std::cout << "Number of used memory Fragments: " << usedMemFragmentsLL.countNodes() << std::endl;
    };

    int countFreeFragments() {
        return freeMemFragmentsLL.countNodes();
    };
    
    void showLists(){
        std::cout <<"Used Memory: "<< std::endl;
        usedMemFragmentsLL.traverseList();
        std::cout <<"Free Memory: "<< std::endl;
        freeMemFragmentsLL.traverseList();
    };
};

/*const MemManage& MemManage::operator=(const MemManage& memoryBlock){
        (*this).size=memoryBlock.size;
        
        // Copies array
        for (int i=0; i<(*this).size; i++){
            (*this).memoryArray[i]=memoryBlock.memoryArray[i];
        }
        
        //Copies lists
        memoryBlock.freeMemFragmentsLL.copyList((*this).freeMemFragmentsLL); // Copies all nodes
        memoryBlock.freeMemFragmentsLL.deleteNode(memoryBlock.freeMemFragmentsLL.getHead()); // deletes first node (empty due to constructor)
        
        memoryBlock.usedMemFragmentsLL.copyList((*this).usedMemFragmentsLL);
        memoryBlock.usedMemFragmentsLL.deleteNode((*this).usedMemFragmentsLL.getHead());
        return memoryBlock;
    }; 

 std::ostream& operator<<(std::ostream& os, const MemManage& memoryBlock){
     for (int i = 0; i < 10; i++) {
            os<< memoryBlock.memoryArray[i]<<" ";             
        }
     return os;
}*/