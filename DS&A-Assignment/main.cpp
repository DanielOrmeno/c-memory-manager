/* 
 * File:   main.cpp
 * Author: Daniel Ormeño - S2850944
 * Data Structures & Algorithms Assignment 1
 * Main
 */

#include <cstdlib>
#include "MemManager.cpp"

using namespace std;

int main(int argc, char** argv) {
    
    cout<< "Welcome, Block of Memory: "<<endl;
    cout<<"\n";
    MemManage aBlockOfMemory (100); // A block of memory of size 100 Bytes (Char)
    cout<<"\n";
    
    char * ptrs[20]={0};
    char * strgs[]= {"zero","one","two","three","four","five","six","seven",
    "eight","nine","ten","sixteen","eighteen","nil","twenty","seventy three"};
    
    
    for (int i=0; i<=10;i++){
        strcpy(ptrs[i]=(char*)aBlockOfMemory.Alloc(1+strlen(strgs[i])),strgs[i]);
        cout<<"\n";
    }
    printf("\nFree Space = %d\n", aBlockOfMemory.Avail());
    
    cout<< "Current Status of Memory: "<<endl;
    aBlockOfMemory.countFragments();
    cout<<"Number of available free Bytes: " << aBlockOfMemory.Avail() << endl;
    cout<<"Number of used Bytes: " << aBlockOfMemory.used() << endl;
    cout<<"\n";
    
    aBlockOfMemory.Dump();
    cout<<"\n";
    
    cout<< "Implementing function Free"<<endl;
    
    aBlockOfMemory.Free(ptrs[1]);
    aBlockOfMemory.Free(ptrs[3]);
    aBlockOfMemory.Free(ptrs[5]);
    aBlockOfMemory.Free(ptrs[7]);
    aBlockOfMemory.Free(ptrs[9]);
    
    cout<< "\nCurrent Status of Memory: "<<endl;
    aBlockOfMemory.countFragments();
    cout<<"Number of available free Bytes: " << aBlockOfMemory.Avail() << endl;
    cout<<"Number of used Bytes: " << aBlockOfMemory.used() << endl;
    cout<<"\n";
    
    printf("\nFree Space = %d\n", aBlockOfMemory.Avail());
    aBlockOfMemory.Dump();
    cout<<"\n";
    
   cout<< "Implementing function Realloc"<<endl;
   aBlockOfMemory.Realloc(ptrs[6],5);
   aBlockOfMemory.Realloc(ptrs[2],7);
   aBlockOfMemory.Realloc(ptrs[4],6);
   
   cout<< "\nCurrent Status of Memory: "<<endl;
    aBlockOfMemory.countFragments();
    cout<<"Number of available free Bytes: " << aBlockOfMemory.Avail() << endl;
    cout<<"Number of used Bytes: " << aBlockOfMemory.used() << endl;
    cout<<"\n";
    
    cout<<"Implementing Compact"<<endl;
    aBlockOfMemory.Compact();
    
    cout<< "\nCurrent Status of Memory: "<<endl;
    aBlockOfMemory.countFragments();
    cout<<"Number of available free Bytes: " << aBlockOfMemory.Avail() << endl;
    cout<<"Number of used Bytes: " << aBlockOfMemory.used() << endl;
    cout<<"\n";
    
        return 0;
}

