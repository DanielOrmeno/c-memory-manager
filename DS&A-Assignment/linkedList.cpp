/* 
 * File:   linkedList.cpp
 * Author: Daniel Ormeño - S2850944
 * Griffith University - Gold Coast Campus
 * Data Structures & Algorithms Assignment 1
 * Linked list class & Node data structure
 */
#include <iostream>

template <typename T>
struct node {
    int segmentSize;
    T * segmentPtr;
    node <T> * nextNode; // Pointer to next Node
};

class linkedList {
public:
    bool unUsed;
private:
    node <char> * head;
    node <char> * tempPtr;

    // Constructors
public:

    linkedList() {
        head = new node <char>;
        head->nextNode = NULL;
        unUsed = true;
    }

    // Accessors

    node <char> * getHead() {
        return head;
    }
    
    // Mutators

    void setHead(node <char> * newHead) {
        head=newHead;
    }
    // Methods

    void traverseList() { // FOR TESTING
        tempPtr = head; // Temporary Pointer points to first node
            while (tempPtr != NULL) {
                std::cout <<"Size: "<< tempPtr->segmentSize << std::endl;
                std::cout <<"Address: "<<static_cast<void*>(tempPtr->segmentPtr) << std::endl;
                std::cout <<"\n";
                tempPtr = tempPtr->nextNode;
            }
    };

    // Counts number of nodes in the list

    int countNodes() {
        tempPtr = head; // Temporary Pointer points to first node
        int count = 0;
        if (unUsed) {
            return 0;
        } else {
            while (tempPtr != NULL) {
                count++;
                tempPtr = tempPtr->nextNode;
            }
            return count;
        }
    };
    
    //Merges two consecutive nodes into one.
    void mergeNodes(){
        tempPtr=head;
        
        node <char> * aux=NULL;
        char * auxPtr=NULL;
        int a;
        bool b=false;
        
        while (tempPtr!=NULL){
            
            std::cout<<"Address of node "<<static_cast<void*>(tempPtr->segmentPtr)<<std::endl;
            auxPtr=tempPtr->segmentPtr+(tempPtr->segmentSize);
            std::cout<<"Address of consecutive node "<<static_cast<void*>(auxPtr)<<std::endl;
            aux=findNode(auxPtr);
            if(aux!=NULL){
                a=aux->segmentSize;
                deleteNode(aux);
                tempPtr->segmentSize=tempPtr->segmentSize+a;
                b=true;
            }
            if (!b)
            tempPtr= tempPtr->nextNode;
            else tempPtr=NULL;
        }
    };

    // Returns memory total size of segments of the list (each node is a segment)

    int sizeOfMemoryFragments() {
        int size = 0;
        tempPtr = head; // Temporary Pointer points to first node
        while (tempPtr != NULL) {
            size = size + tempPtr->segmentSize;
            tempPtr = tempPtr->nextNode;
        }
        return size;
    };

    // Finds Node based on segmentPtr returns pointer to fragment

    node <char> * findNode(void* ptr) {
        tempPtr = head; // Temporary Pointer points to first node
        node <char> * result = NULL;
        while (tempPtr != NULL) {
            if (tempPtr->segmentPtr == ptr) {
                result = tempPtr;
            }
            tempPtr = tempPtr->nextNode;
        }
        return result;
    };

    // Updates Node

    void updateNode(node <char> * nodePtr, int sizeOfFragment, char * memAddress) {
        tempPtr = nodePtr; // Temporary Pointer points to first node
        if (tempPtr != NULL) {
            tempPtr->segmentSize = sizeOfFragment;
            tempPtr->segmentPtr = memAddress; //Pointer to first element (array) of fragment
        }
    };

    // Inserts node at the end of the list

    void insertNodeAtEnd(int sizeOfFragment, char * memAddress) {
        // Traverse list
        tempPtr = head; // Temporary Pointer points to first node
            while (tempPtr->nextNode != NULL) {
                tempPtr = tempPtr->nextNode;
            } // Gets to the end of the list
            tempPtr->nextNode = new node <char>; // Creates new node at end of the list
            tempPtr = tempPtr->nextNode; // Moves temp pointer to new node
            tempPtr->nextNode = NULL; // Enforces NULL on last node
            tempPtr->segmentSize = sizeOfFragment;
            tempPtr->segmentPtr = memAddress; //Pointer to first element (array) of fragment
    };

    // Inserts node at the beginning of the list

    void insertNodeAtBeginning(int sizeOfFragment, char * memAddress) {
        // Traverse list
        tempPtr = head; // Temporary Pointer points to first node
        head = new node <char>; // Creates new node to be inserted
        head->nextNode = tempPtr;
        head->segmentSize = sizeOfFragment;
        head->segmentPtr = memAddress; //Pointer to first element (in array) of fragment
    };

    // Deletes node from Linked List

    void deleteNode(node <char> * nodePtr) {
        tempPtr = head;
        node <char> * auxPtr;
        if (tempPtr == nodePtr) {
            head = tempPtr->nextNode;
            delete nodePtr;
        } else {

            while (tempPtr != NULL) {
                if (tempPtr->nextNode == nodePtr) {
                    auxPtr = nodePtr->nextNode;
                    tempPtr->nextNode = auxPtr;
                    delete nodePtr; //CHECK IF ACTUALLY DELETES NODE FROM REAL MEMORY
                    break;
                }
                tempPtr = tempPtr->nextNode;
            }
        }
    };
    
    // Deletes all nodes.
    void deleteAll() {
        tempPtr = head;
        node <char>*aux;
            while (tempPtr != NULL) {
                aux=tempPtr;
                tempPtr = tempPtr->nextNode;
                deleteNode(aux);
            }
        };
        
    // Copies a linked list
        void copyList( linkedList &aList){
            tempPtr=head;
            node <char> * tempPtr2=aList.head;
            while (tempPtr !=NULL){
                (*this).insertNodeAtEnd(tempPtr2->segmentSize, tempPtr2->segmentPtr);
                tempPtr=tempPtr->nextNode;
                tempPtr=tempPtr2->nextNode;
            }
        };
    
    // Checks and compares the size of each segment and returns pointer to the node

    node <char> * smallestMemoryFragment(int size) {

        tempPtr = head; // Temporary Pointer points to first node
        int tempSize = 999; // size of potential memory fragment to be used
        node <char> * fragPtr = NULL;

        while (tempPtr != NULL) {
            if (size == tempPtr->segmentSize) { //If same size return pointer to memory segment.
                fragPtr = tempPtr;
                break;
            } else if (size < tempPtr->segmentSize) {
                if (tempPtr->segmentSize < tempSize) {
                    tempSize = tempPtr->segmentSize; // Uses temporary variables to find the smallest usable memory fragment
                    fragPtr = tempPtr;
                }
            }
            tempPtr = tempPtr->nextNode;
        } // end of while
        std::cout << "Address of available memory: " << static_cast<void*> (fragPtr->segmentPtr) << std::endl;
        std::cout << "Size of available memory in fragment: " << tempSize << std::endl;
        std::cout << "Size of fragment to be allocated: " << size << std::endl;
        return (fragPtr);
    };
    
    char* largestMemoryAddress(char * arrayAddress){
        tempPtr = head; // Temporary Pointer points to first node
        char * max=arrayAddress;
            while (tempPtr != NULL) {
                if (tempPtr->segmentPtr>max){
                    max=tempPtr->segmentPtr;
                }
                tempPtr = tempPtr->nextNode;
            }
        return max;
    }
};